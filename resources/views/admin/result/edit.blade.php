@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > results</div>
                    <div class="panel-body">
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findResult')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <form role="form" method="POST" action="{{route("admin-results.update", ["id" => $match->id])}}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="put" />
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Match</th>
                                <th>Game date</th>
                                <th>League</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        {{$match->home->club_name}} - {{$match->guest->club_name}}
                                        <input type="hidden" name="home_id" value="{{$match->home->id}}" />
                                        <input type="hidden" name="guest_id" value="{{$match->guest->id}}" />
                                    </td>
                                    <td>{{$match->game_date}}</td>
                                    <td>{{$match->league->league}}</td>
                                    <td>
                                        <select class="form-control" name="result">
                                            @foreach($results as $result)
                                                @if($match->result->result == $result->id)
                                                    <option value="{{$result->id}}" selected>{{$result->result_name}}</option>
                                                @else
                                                    <option value="{{$result->id}}">{{$result->result_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                          </table>
                            <input class="btn btn-primary" type="submit" value="Save">
                        </form>
                    </div>
                </div>
            </div>
@endsection
