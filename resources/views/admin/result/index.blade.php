@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > results</div>
                    <div class="panel-body">
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findResult')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Match</th>
                                <th>Game date</th>
                                <th>League</th>
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($matches as $match)
                                <tr>
                                    <td>
                                        {!!$match->result->result == 2 ? '<b>'.$match->home->club_name.'</b>' : $match->home->club_name!!} - {!!$match->result->result == 3 ? '<b>'.$match->guest->club_name.'</b>' : $match->guest->club_name!!}
                                    </td>
                                    <td>{{$match->game_date}}</td>
                                    <td>{{$match->league->league}}</td>
                                    <td>{{$match->result->resultName->result_name}}</td>
                                    <td>
                                        <a href="{{route('admin-results.edit', ['id' => $match->id])}}" class="btn btn-success"><i class="glyphicon glyphicon-pencil"></i>  Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                        {{ $matches->links() }}
                    </div>
                </div>
            </div>
@endsection
