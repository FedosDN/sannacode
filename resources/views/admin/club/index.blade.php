@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > clubs</div>
                    <div class="panel-body">
                        <form style="width:30%; float:left">
                            <button class="btn btn-primary" type="submit">Add Club</button>
                        </form>
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findClub')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>N</th>
                                <th>Club</th>
                                <th>League</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($clubs as $club)
                                <tr>
                                    <td>{{ $club->id }}</td>
                                    <td>{!! $club->club_name !!}</td>
                                    <td>{!! $club->league->league !!}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
@endsection
