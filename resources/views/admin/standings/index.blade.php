@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > clubs</div>
                    <div class="panel-body">
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findClub')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <table class="table">
                            @foreach($leagues as $league)
                            <thead>
                                <tr>
                                    <th colspan="9">
                                        {{$league->league}}
                                    </th>
                                </tr>
                              <tr>
                                <th>Position</th>
                                <th>Club</th>
                                <th>Matches</th>
                                <th>Win</th>
                                <th>Draw</th>
                                <th>Defeat</th>
                                <th>Goal scored</th>
                                <th>Goal missed</th>
                                <th>Score</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($standings as $standing)
                                @if($league->id == $standing->clubs->league_id)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $standing->clubs->club_name }}</td>
                                        <td>{!! $standing->matches_count !!}</td>
                                        <td>{!! $standing->win !!}</td>
                                        <td>{!! $standing->draw !!}</td>
                                        <td>{!! $standing->defeat !!}</td>
                                        <td>{!! $standing->goals_scored !!}</td>
                                        <td>{!! $standing->goals_missed !!}</td>
                                        <td>{!! $standing->score !!}</td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            @endforeach
                          </table>
                    </div>
                </div>
            </div>
@endsection
