<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin Dashboard</div>

                    <div class="panel-body">
                        <ul class="nav nav-sidebar">
                            <li class="{{strpos(Route::currentRouteName(), 'admin-match.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-match.index')}}">Matches <span class="sr-only"></span></a></li>
                            <li class="{{strpos(Route::currentRouteName(), 'admin-results.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-results.index')}}">Results <span class="sr-only"></span></a></li>
                            <li class="{{strpos(Route::currentRouteName(), 'admin-standings.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-standings.index')}}">Standings <span class="sr-only"></span></a></li>
                            <li class="{{strpos(Route::currentRouteName(), 'admin-clubs.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-clubs.index')}}">Clubs <span class="sr-only"></span></a></li>
                            <li class="{{strpos(Route::currentRouteName(), 'admin-leagues.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-leagues.index')}}">Leagues <span class="sr-only"></span></a></li>
                            <li class="{{strpos(Route::currentRouteName(), 'admin-users.') === 0 ? 'active' : ''}}"><a href="{{ route('admin-users.index')}}">Users <span class="sr-only"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>

@yield('body')
        </div>
    </div>
    </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ asset('js/function.js') }}"></script>
    <script>
$("#add_match").click(function(e){
    e.preventDefault();
    var fd = new FormData();
    fd.append('home_opponent', $('#home_opponent').val());
    fd.append('guest_opponent', $('#guest_opponent').val());
    fd.append('_token', "{{csrf_token()}}");
    fd.append('game_date', $('#datepicker').val());
    var request = new XMLHttpRequest();
    request.open("POST", "{{route("admin-match.store")}}");
    request.send(fd);
    window.location.replace("{{route("admin-match.index")}}");
});
</script>
</body>
</html>
