@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > leagues</div>
                    <div class="panel-body">
                        <form style="width:30%; float:left">
                            <button class="btn btn-primary" type="submit">Add League</button>
                        </form>
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findLeague')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>N</th>
                                <th>League</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($leagues as $league)
                                <tr>
                                    <td>{{ $league->id }}</td>
                                    <td>{!! $league->league !!}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
@endsection
