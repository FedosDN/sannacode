@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > matches > create</div>
                    <div class="panel-body">
                        <div>
                            @if ($errors->has('msg'))
                                <div class="alert" role="alert"><strong>{{ $errors->first('msg') }}</strong></div>
                            @endif
                        </div>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>League</th>
                                <th>Home Opponent</th>
                                <th>Guest Opponent</th>
                                <th>Game date</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <form method="post" action="{{route("admin-select_league")}}">
                                            {{ csrf_field() }}
                                            <select name="league" onchange="submit()">
                                                @foreach($leagues as $league)
                                                    @if(isset($leagueSelectedId) && $league->id == $leagueSelectedId)
                                                        <option value="{{$league->id}}" selected>{{$league->league}}</option>
                                                    @else
                                                        <option value="{{$league->id}}">{{$league->league}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </form>
                                    </td>
<!--                                    <form id="match_add" method="post" action="{{route('admin-match.store')}}">-->
                                    <td>
                                        {{ csrf_field() }}
                                        <select id="home_opponent">
                                            @foreach($clubs as $club)
                                                <option value="{{$club->id}}">{{$club->club_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select id="guest_opponent">
                                            @foreach($clubs as $club)
                                                <option value="{{$club->id}}">{{$club->club_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id='datepicker' name='match_date' required value="{{date('d/m/Y')}}">
                                        <p></p>
                                        <button class="btn btn-success" id="add_match">Add Match</button>
                                    </td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>


@endsection
