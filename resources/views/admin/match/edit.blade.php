@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > matches > create</div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{route("admin-match.update", ["id" => $game->id])}}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="put" />
                         <table class="table">
                            <thead>
                              <tr>
                                <th>League</th>
                                <th>Home Opponent</th>
                                <th>Guest Opponent</th>
                                <th>Game date</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input class="form-control"  value="{{$league->league}}" disabled>
                                    </td>
                                    <td>
                                        <select class="form-control" name="home_opponent">
                                            @foreach($clubs as $club)
                                                @if($club->club_name == $game->home_opponent)
                                                    <option value="{{$club->id}}" selected>{{$club->club_name}}</option>
                                                @else
                                                    <option value="{{$club->id}}">{{$club->club_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="guest_opponent">
                                            @foreach($clubs as $club)
                                                @if($club->club_name == $game->guest_opponent)
                                                    <option value="{{$club->id}}" selected>{{$club->club_name}}</option>
                                                @else
                                                    <option value="{{$club->id}}">{{$club->club_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control" value="{{$game->game_date}}" type="text" id='datepicker' name='match_date'>
                                    </td>
                                </tr>
                            </tbody>
                          </table>
                            <input class="btn btn-primary" type="submit" value="Save">
                         </form>
                    </div>
                </div>
            </div>


@endsection
