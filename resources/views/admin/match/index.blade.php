@extends('admin.layouts.app')

@section('body')
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">admin > matches</div>
                    <div class="panel-body">
                        <form style="width:30%; float:left" action="{{route('admin-match.create')}}">
                            <button class="btn btn-primary" type="submit">Add Match</button>
                        </form>
                        <form style="width:70%; float:left" method="get" action="{{route('admin-findMatch')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        @if ($errors->has('msg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('msg') }}</strong>
                                    </span>
                        @endif
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Match</th>
                                <th>Game date</th>
                                <th>League</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($matches as $match)
                                <tr>
                                    <td>{{$match->home->club_name}} - {{$match->guest->club_name}}</td>
                                    <td>{{$match->game_date}}</td>
                                    <td>{{$match->league->league}}</td>
                                    <td>
                                        <a href="{{route('admin-match.edit', ['id' => $match->id])}}" class="btn btn-success"><i class="glyphicon glyphicon-pencil"></i>  Edit</a>
                                        <form action="{{route('admin-match.destroy', ['id' => $match->id])}}" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="_method" value="delete" />
                                            <input class="btn btn-danger" type="submit" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                        {{ $matches->links() }}
                    </div>
                </div>
            </div>
@endsection
