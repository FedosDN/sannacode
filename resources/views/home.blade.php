@extends('layouts.app')

@section('matches')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Matches</div>
                    <div class="panel-body">
                        <form style="width:30%; float:left" method="get" action="{{route('findMatch')}}">
                          <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Search">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Match</th>
                                <th>Game date</th>
                                <th>League</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($matches as $match)
                                <tr>
                                    <td>{{$match->home->club_name}} - {{$match->guest->club_name}}</td>
                                    <td>{{$match->game_date}}</td>
                                    <td>{{$match->league->league}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                        {{ $matches->links() }}
                    </div>
                </div>
            </div>
@endsection

@section('standings')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Standings</div>
                    <div class="panel-body">
                        <table class="table">
                            @foreach($leagues as $league)
                            <thead>
                                <tr>
                                    <th colspan="9">
                                        {{$league->league}}
                                    </th>
                                </tr>
                              <tr>
                                <th>Position</th>
                                <th>Club</th>
                                <th>Matches</th>
                                <th>Win</th>
                                <th>Draw</th>
                                <th>Defeat</th>
                                <th>Goal scored</th>
                                <th>Goal missed</th>
                                <th>Score</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($standings as $standing)
                                @if($league->id == $standing->clubs->league_id)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $standing->clubs->club_name }}</td>
                                        <td>{!! $standing->matches_count !!}</td>
                                        <td>{!! $standing->win !!}</td>
                                        <td>{!! $standing->draw !!}</td>
                                        <td>{!! $standing->defeat !!}</td>
                                        <td>{!! $standing->goals_scored !!}</td>
                                        <td>{!! $standing->goals_missed !!}</td>
                                        <td>{!! $standing->score !!}</td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            @endforeach
                          </table>
                    </div>
                </div>
            </div>
@endsection

@section('clubs')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Clubs</div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                              <tr>
                                <th>N</th>
                                <th>Club</th>
                                <th>League</th>
                                <th>Favourite</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($clubs as $club)
                                <tr>
                                    <td>{{ $club->id }}</td>
                                    <td>{!! $club->club_name !!}</td>
                                    <td>{!! $club->league->league !!}</td>
                                        <td>
                                            @if(!$club->favourite->isEmpty())
                                                <a href="/remove-favourite/{{$club->id}}" class="btn btn-danger">Remove</a>
                                            @else
                                                <a href="/add-favourite/{{$club->id}}" class="btn btn-success">Add</a>
                                            @endif
                                        </td>
                                </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
@endsection
