#Introduction
```
composer install
```
```
sudo chmod -R 0777 storage bootstrap
```
```
composer dumpautoload
php artisan migrate
php artisan db:seed
```

#### default user
* login: admin@admin.com
* password: admin

---
after all, need to run cron for notifications to users
run via crontab -e command

```
0 0 * * * php /path/to/artisan cron:notify >> /dev/null 2>&1
```