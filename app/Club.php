<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'club_name', 'league_id'
    ];

    public function league()
    {
        return $this->hasOne(League::class, 'id', 'league_id');
    }

    public function favourite()
    {
        return $this->belongsToMany(User::class, 'club_favourite', 'club_id', 'user_id');
    }
}
