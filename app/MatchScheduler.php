<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchScheduler extends Model
{
    public $timestamps = false;

    protected $table = 'match_scheduler';

    protected $fillable = [
        'home_opponent', 'guest_opponent', 'game_date', 'league_id'
    ];

    public function league()
    {
        return $this->hasOne(League::class, 'id', 'league_id');
    }

    public function result()
    {
        return $this->hasOne(MatchResults::class, 'schedule_id', 'id');
    }

    public function home()
    {
        return $this->hasOne(Club::class, 'id', 'home_opponent');
    }

    public function guest()
    {
        return $this->hasOne(Club::class, 'id', 'guest_opponent');
    }
}
