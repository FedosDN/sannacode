<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MatchScheduler;
use App\Standings;
use App\League;
use App\Club;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = MatchScheduler::with('home', 'guest')
            ->paginate(5);

        $standings = Standings::with('clubs.league')->orderBy('score', 'desc')->get();
        $leagues   = League::all();

        $userId = Auth::user()->id;

        $clubs = Club::with('league')
            ->with(['favourite' => function($q) use ($userId) {
                $q->where('user_id', $userId);
            }])
            ->paginate(10);
//dd($clubs);
        return view('home', compact('matches', 'standings', 'leagues', 'clubs', 'favourite'));
    }

    public function search(Request $request)
    {
        $matches = MatchScheduler::with(['home' => function($q) use ($request) {
            $q->where('club_name', 'like', '%' . $request->q . '%');
        }])->with(['guest' => function($q) use ($request) {
            $q->where('club_name', 'like', '%' . $request->q . '%');
        }])->paginate(10);

        foreach ($matches as $match) {
            if (isset($match->home)) {
                $search[] = $match->id;
            }
            if (isset($match->guest)) {
                $search[] = $match->id;
            }
        }

        $matches = MatchScheduler::with('home', 'guest')
            ->whereIn('id', $search ?? [])
            ->paginate(5);

        $standings = Standings::with('clubs.league')->orderBy('score', 'desc')->get();
        $leagues   = League::all();
        $clubs = Club::with('league', 'favourite')->paginate(10);

        return view('home', compact('matches', 'standings', 'leagues', 'clubs'));
    }

    public function addToFavourite(Request $request)
    {
        $favourite = User::find(Auth::user()->id);
        $favourite->favourite()->attach($request->id);

        return redirect()->back();
    }

    public function removeFromFavourite(Request $request)
    {
        $favourite = User::find(Auth::user()->id);
        $favourite->favourite()->detach($request->id);

        return redirect()->back();
    }
}
