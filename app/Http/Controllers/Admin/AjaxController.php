<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\League;
use App\Club;

class AjaxController extends Controller
{
    public function selectLeague(Request $request)
    {
        $league = League::where('id', $request->league_id)->first();
        $clubs = Club::whereHas('league', function($q) use ($league) {
            $q->where('league_id', $league->id);
        })->get();

        return compact('league', 'clubs');
    }
}
