<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MatchScheduler;
use App\MatchResults;
use App\Result;
use App\Standings;
use Carbon\Carbon;

class ResultController extends Controller
{
    /**
     * match statuses
     * from results table
     */
    const NOT_PLAYERD = 1;
    const HOME_WIN    = 2;
    const GUEST_WIN   = 3;
    const DRAW        = 4;
    const PLAYING     = 5;
    const CANCELED    = 6;

    /**
     * match scores
     */
    const WIN_SCORE    = 3;
    const DRAW_SCORE   = 1;
    const DEFEAT_SCORE = 0;

    /**
     * workaround
     * making APP more simple, so adding goals by default
     */
    const GOAL = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = MatchScheduler::with('result', 'league')->paginate(10);

        return view('admin.result.index', compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $match            = MatchScheduler::with('league', 'result.resultName')->find($id);
        $match->game_date = Carbon::createFromFormat('Y-m-d H:i:s', $match->game_date)->format('d/m/Y');

        $results = Result::all();

        return view('admin.result.edit', compact('match', 'results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = MatchResults::where('schedule_id', $id)->first();
        $result->result = $request->result;
        $result->save();

//        $home =
//        $result->schedule_id;
        if ($request->result == self::HOME_WIN) {
            //add scores for home
            $standings = Standings::where('club_id', $request->home_id)->first();
            $standings->matches_count++;
            $standings->win++;
            $standings->goals_scored += self::GOAL;
            $standings->score        += self::WIN_SCORE;
            $standings->save();

            //add scores for guest
            $standings = Standings::where('club_id', $request->guest_id)->first();
            $standings->matches_count++;
            $standings->defeat++;
            $standings->goals_missed += self::GOAL;
            $standings->score        += self::DEFEAT_SCORE;
            $standings->save();
        }

        if ($request->result == self::GUEST_WIN) {
            //add scores for home
            $standings = Standings::where('club_id', $request->guest_id)->first();
            $standings->matches_count++;
            $standings->win++;
            $standings->goals_scored += self::GOAL;
            $standings->score        += self::WIN_SCORE;
            $standings->save();

            //add scores for guest
            $standings = Standings::where('club_id', $request->home_id)->first();
            $standings->matches_count++;
            $standings->defeat++;
            $standings->goals_missed += self::GOAL;
            $standings->score        += self::DEFEAT_SCORE;
            $standings->save();
        }

        if ($request->result == self::DRAW) {
            //add scores for home
            $standings = Standings::where('club_id', $request->guest_id)->first();
            $standings->matches_count++;
            $standings->draw++;
            $standings->goals_scored += self::GOAL;
            $standings->goals_missed += self::GOAL;
            $standings->score        += self::DRAW_SCORE;
            $standings->save();

            //add scores for guest
            $standings = Standings::where('club_id', $request->home_id)->first();
            $standings->matches_count++;
            $standings->draw++;
            $standings->goals_scored += self::GOAL;
            $standings->goals_missed += self::GOAL;
            $standings->score        += self::DRAW_SCORE;
            $standings->save();
        }
        return redirect()->to(route('admin-results.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $matches = MatchScheduler::with('result', 'league')
            ->where('home_opponent', 'like', '%' . $request->q)
            ->orWhere('guest_opponent', 'like', '%' . $request->q)
            ->paginate(10);

        return view('admin.result.index', compact('matches'));
    }
}
