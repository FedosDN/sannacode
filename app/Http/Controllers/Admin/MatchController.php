<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MatchResults;
use App\MatchScheduler;
use App\Club;
use App\League;
use Carbon\Carbon;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = MatchScheduler::with('home', 'guest')
            ->paginate(10);
        return view('admin.match.index', compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $leagues = League::all();

        if (isset($request->selectedLeagueId) && is_numeric($request->selectedLeagueId)) {
            $clubs   = Club::whereHas('league', function($q) use ($request){
                $q->where('league_id', $request->selectedLeagueId);
            })->get();
        } else {
            //getting default league
            $clubs   = Club::whereHas('league', function($q) use ($leagues){
                $q->where('league_id', $leagues[0]->id);
            })->get();
        }
        $leagueSelectedId = $request->selectedLeagueId ?? null;

        return view('admin.match.create', compact('leagues', 'clubs', 'leagueSelectedId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->home_opponent == $request->guest_opponent) {
            return Redirect::back()->withErrors(['msg', 'Clubs cannot be the same']);
        }

        $game = new MatchScheduler;

        $home  = Club::where('id', $request->home_opponent)->first();
        $guest = Club::where('id', $request->guest_opponent)->first();

        $game->home_opponent  = $home->id;
        $game->guest_opponent = $guest->id;
        //TODO: fix that:
        $date = $request->game_date." ".date("H:i:s");
        $game_date = Carbon::createFromFormat('d/m/Y H:i:s', $date)->toDateTimeString();
        $game->game_date      = $game_date; //workaround. no time to do datetime picker
        $game->league_id      = $home->league_id;

        if ($game->save()) {
            $result              = new MatchResults;
            $result->schedule_id = $game->id;
            $result->result      = 1;
            $result->save();
        }

        return redirect()->to(route('admin-match.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game    = MatchScheduler::find($id);
        $league  = League::find($game->league_id);
        $clubs   = Club::whereHas('league', function($q) use ($game){
            $q->where('league_id', $game->league_id);
        })->get();

        $game->game_date = Carbon::createFromFormat('Y-m-d H:i:s', $game->game_date)->format('d/m/Y');

        return view('admin.match.edit', compact('league', 'clubs', 'game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->home_opponent == $request->guest_opponent) {
            return Redirect::back()->withErrors(['msg', 'Clubs cannot be the same']);
        }

        $game    = MatchScheduler::find($id);

        $date                 = $request->match_date." ".date("H:i:s");
        $game_date            = Carbon::createFromFormat('d/m/Y H:i:s', $date)->toDateTimeString();

        $home  = Club::where('id', $request->home_opponent)->first();
        $guest = Club::where('id', $request->guest_opponent)->first();

        $game->home_opponent  = $home->id;
        $game->guest_opponent = $guest->id;
        $game->save();

        return redirect()->to(route('admin-match.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MatchScheduler::destroy($id);

        return redirect()->to(route('admin-match.index'));
    }

    public function selectLeague(Request $request)
    {
        $league  = League::where('id', $request->league)->first();

        return redirect()->to(route('admin-match.create', ['selectedLeagueId' => $league->id]));
//        return view('admin.match.create', compact('leagues', 'clubs'));
    }

    public function search(Request $request)
    {
        $matches = MatchScheduler::with(['home' => function($q) use ($request) {
            $q->where('club_name', 'like', '%' . $request->q . '%');
        }])->with(['guest' => function($q) use ($request) {
            $q->where('club_name', 'like', '%' . $request->q . '%');
        }])->paginate(10);

        foreach ($matches as $match) {
            if (isset($match->home)) {
                $search[] = $match->id;
            }
            if (isset($match->guest)) {
                $search[] = $match->id;
            }
        }

        $matches = MatchScheduler::with('home', 'guest')
            ->whereIn('id', $search ?? [])
            ->paginate(10);

        return view('admin.match.index', compact('matches'));
    }
}
