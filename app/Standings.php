<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standings extends Model
{
    public $timestamps = false;

    protected $table = 'standings';

    protected $fillable = [
        'matches_count', 'win', 'draw', 'defeat', 'goals_scored', 'goals_missed', 'score', 'club_id'
    ];

    public function clubs()
    {
        return $this->hasOne(Club::class, 'id', 'club_id');
    }
}
