<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchResults extends Model
{
    public $timestamps = false;

    protected $table = 'match_result';

    protected $fillable = [
        'schedule_id', 'result'
    ];

    public function resultName()
    {
        return $this->hasOne(Result::class, 'id', 'result');
    }
}
