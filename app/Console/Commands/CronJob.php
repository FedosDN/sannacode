<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Club;
use App\MatchScheduler;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notification to user about his favourite nearby game';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereHas('favourite')
            ->with('favourite')
            ->get()->toArray();

        foreach ($users as $user) {
            if ($user['favourite']) {
                foreach ($user['favourite'] as $favourite) {
                    $match = MatchScheduler::where(function ($q) use ($favourite) {
                        $q->where('home_opponent', $favourite['id'])
                        ->orWhere('guest_opponent', $favourite['id']);
                    })
                    ->first();

                    if ($match) {
                        $time = time();
                        $diff = strtotime($match->game_date) - $time;
                        if($diff <= 86000) {//1 day
                            //todo sendmail
                            //send mail to user
                        }
                    }
                }
            }
        }
    }
}
