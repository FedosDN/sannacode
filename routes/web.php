<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'as' => 'admin-', 'middleware' => 'admin'], function() {
    Route::get('/', 'Admin\AdminController@index');
    Route::resource('match',        'Admin\MatchController',     ['except' => ['show']]);
    Route::resource('results',      'Admin\ResultController',    ['except' => ['show', 'create', 'destroy']]);
    Route::resource('standings',    'Admin\StandingsController', ['except' => ['show', 'create', 'destroy', 'update', 'store']]);
    Route::resource('clubs',        'Admin\ClubsController',     ['except' => ['show']]);
    Route::resource('leagues',      'Admin\LeaguesController',   ['except' => ['show']]);
    Route::resource('users',        'Admin\UsersController',     ['except' => ['show']]);

    Route::post('select_league',    'Admin\MatchController@selectLeague')->name('select_league');
    Route::get('match/search',      'Admin\MatchController@search')->name('findMatch');
    Route::get('results/search',    'Admin\ResultController@search')->name('findResult');
    Route::get('clubs/search',      'Admin\ClubsController@search')->name('findClub');
    Route::get('leagues/search',    'Admin\LeaguesController@search')->name('findLeague');
});

Route::get('home/search',           'HomeController@search')->name('findMatch');
Route::get('test',           'HomeController@test');
Route::get('add-favourite/{id}',    'HomeController@addToFavourite');
Route::get('remove-favourite/{id}', 'HomeController@removeFromFavourite');
