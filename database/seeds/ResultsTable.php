<?php

use Illuminate\Database\Seeder;

class ResultsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('results')->insert([
            'result_name' => 'not_played'
        ]);
        DB::table('results')->insert([
            'result_name' => 'home_win'
        ]);
        DB::table('results')->insert([
            'result_name' => 'guest_win'
        ]);
        DB::table('results')->insert([
            'result_name' => 'draw'
        ]);
        DB::table('results')->insert([
            'result_name' => 'playing'
        ]);
        DB::table('results')->insert([
            'result_name' => 'canceled'
        ]);
    }
}
