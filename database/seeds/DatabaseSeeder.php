<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(LeagueTable::class);
        $this->call(ClubsTable::class);
        $this->call(UsersTable::class);
        $this->call(ResultsTable::class);
        $this->call(StandingsTable::class);
//        $this->call(MatchResultTable::class);
//        $this->call(SchedulerTable::class);
        Model::reguard();
    }
}
