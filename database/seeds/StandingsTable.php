<?php

use Illuminate\Database\Seeder;

class StandingsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('standings')->insert([
            'club_id' => 1
        ]);
        DB::table('standings')->insert([
            'club_id' => 2
        ]);
        DB::table('standings')->insert([
            'club_id' => 3
        ]);
        DB::table('standings')->insert([
            'club_id' => 4
        ]);
        DB::table('standings')->insert([
            'club_id' => 5
        ]);
        DB::table('standings')->insert([
            'club_id' => 6
        ]);
        DB::table('standings')->insert([
            'club_id' => 7
        ]);
        DB::table('standings')->insert([
            'club_id' => 8
        ]);
        DB::table('standings')->insert([
            'club_id' => 9
        ]);
        DB::table('standings')->insert([
            'club_id' => 10
        ]);
    }
}
