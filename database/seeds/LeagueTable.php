<?php

use Illuminate\Database\Seeder;

class LeagueTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leagues')->insert([
            'league' => 'League 1'
        ]);
        DB::table('leagues')->insert([
            'league' => 'League 2'
        ]);
    }
}
