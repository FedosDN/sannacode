<?php

use Illuminate\Database\Seeder;

class UsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email'     => 'admin@admin.com',
            'name'      => 'Admin',
            'password'  => bcrypt('admin'),
            'remember_token' => str_random(60),
            'is_admin'  => true
        ]);
    }
}
