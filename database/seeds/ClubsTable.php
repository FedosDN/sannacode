<?php

use Illuminate\Database\Seeder;

class ClubsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clubs')->insert([
            'club_name' => 'Club 1',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 2',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 3',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 4',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 5',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 6',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 7',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 8',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 9',
            'league_id' => 1
        ]);
        DB::table('clubs')->insert([
            'club_name' => 'Club 10',
            'league_id' => 1
        ]);
    }
}
